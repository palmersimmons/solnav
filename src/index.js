import React from 'react';
import ReactDOM from 'react-dom';
import './static/sass/index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloProvider } from 'react-apollo'

const graphCMSAPI= 'https://api-useast.graphcms.com/v1/ck02gfxwb0u8l01cbaj1r989j/master'

const client = new ApolloClient({
  link: new HttpLink({ uri: graphCMSAPI }),
  cache: new InMemoryCache()
})

ReactDOM.render(
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>,
    document.getElementById('root')
  )
