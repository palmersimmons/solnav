import React from 'react'
import { Link } from 'react-router-dom'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const POSTS_PER_PAGE = 0

const Home = ({ data: { loading, error, posts, postsConnection, networkStatus }, loadMorePosts }) => {
    if (error) return <h1>Error fetching posts!</h1>
    if (posts && postsConnection) {
      const areMorePosts = posts.length < postsConnection.aggregate.count
      return (
        <section>
          <ul className='Home-ul'>
            {posts.map(post => (
              <li className='Home-li' key={`post-${post.id}`}>
                <Link to={`/post/${post.id}`} className='Home-link'>
                  <div className='Home-placeholder'>
                    {posts.thumbnail != null && <img alt={posts.title} className='Home-img' src={posts.thumbnail.url}/>}
                  </div>
                  <h3>{post.title}</h3>
                </Link>
              </li>
            ))}
          </ul>
          <div className='Home-showMoreWrapper'>
            {areMorePosts
              ? <button className='Home-button' disabled={loading} onClick={() => loadMorePosts()}>
                {loading ? 'Loading...' : 'Show More Posts'}
              </button>
              : ''}
          </div>
        </section>
      )
    }
    return <h2>Loading posts...</h2>
  }

  export const posts = gql`
  query posts{
    posts{
        id
        title
        createdAt
        detail {
            raw
            html
        }
        thumbnail {
            url
        }
        provider {
            id
            name
        }
        postType {
            name
        }
},
    postsConnection {
      aggregate {
        count
      }
    }
  }
`

export const postsQueryVars = {
    skip: 0,
    first: POSTS_PER_PAGE
  }
  
  export default graphql(posts, {
    options: {
      variables: postsQueryVars
    },
    props: ({ data }) => ({
      data,
      loadMorePosts: () => {
        return data.fetchMore({
          variables: {
            skip: data.posts.length
          },
          updateQuery: (previousResult, { fetchMoreResult }) => {
            if (!fetchMoreResult) {
              return previousResult
            }
            return Object.assign({}, previousResult, {
              posts: [...previousResult.posts, ...fetchMoreResult.posts]
            })
          }
        })
      }
    })
  })(Home)